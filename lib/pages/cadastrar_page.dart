import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;

import '../helpers/api_url.dart';

class CadastrarPage extends StatefulWidget {
  const CadastrarPage({super.key});

  @override
  State<CadastrarPage> createState() => _CadastrarPageState();
}

class _CadastrarPageState extends State<CadastrarPage> {
  final _formkey = GlobalKey<FormState>();
  TextEditingController nomeController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmePasswordController = TextEditingController();

  XFile? _avatarImagem;

  void _pickedImage(ImageSource source) async {
    final picker = ImagePicker();
    final pickerImage = await picker.pickImage(source: source);

    if (pickerImage != null) {
      setState(() {
        _avatarImagem = XFile(pickerImage.path);
      });
    }
  }

  void _showImagePicker() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return SafeArea(
          child: SizedBox(
            height: 150,
            child: Wrap(
              children: [
                ListTile(
                  title: const Text('Camera'),
                  leading: const Icon(Icons.camera),
                  onTap: () {
                    Navigator.of(context).pop();
                    _pickedImage(ImageSource.camera);
                  },
                ),
                ListTile(
                  title: const Text('Galeria'),
                  leading: const Icon(Icons.photo),
                  onTap: () {
                    Navigator.of(context).pop();
                    _pickedImage(ImageSource.gallery);
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> cadastrarUser() async {
    if (_formkey.currentState!.validate()) {
      var request = http.MultipartRequest(
          'POST', Uri.parse("${ApiUrl.baseUrl}/cadastrar"));
      request.fields['nome'] = nomeController.text;
      request.fields['email'] = emailController.text;
      request.fields['password'] = passwordController.text;

      if (_avatarImagem != null) {
        request.files.add(
          await http.MultipartFile.fromPath("imagem", _avatarImagem!.path),
        );
      }

      var response = await request.send();
      var streamedResponse = await http.Response.fromStream(response);
      debugPrint(streamedResponse.body);

      //var jsonData = jsonDecode(streamedResponse.body);

      if (response.statusCode == 200) {
        if (context.mounted) {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: const Text('Cadastro realizado!'),
                content: const Text('Usuario cadastrado com sucesso!'),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                    child: const Text('OK'),
                  ),
                ],
              );
            },
          );
        }
      } else {
        if (context.mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                  'Ocorreu um erro ao cadastrar. Por favor tente novamente mais tarde!'),
            ),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Cadastrar"),
        backgroundColor: const Color(0xffF98E1E),
        elevation: 0,
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Form(
            key: _formkey,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: _showImagePicker,
                    child: (_avatarImagem == null)
                        ? Container(
                            height: 100,
                            width: 100,
                            decoration: const BoxDecoration(
                                color: Color(0xFFAAAAAA),
                                shape: BoxShape.circle),
                            child: const Icon(
                              Icons.add_a_photo,
                              color: Color(0xFFffffff),
                              size: 30,
                            ),
                          )
                        : Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: FileImage(File(_avatarImagem!.path)),
                              ),
                            ),
                          ),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    controller: nomeController,
                    decoration: const InputDecoration(labelText: "Nome"),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Por favor, informe um nome";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    controller: emailController,
                    decoration: const InputDecoration(labelText: "E-mail"),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Por favor, informe um e-mail!";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(labelText: "Senha"),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Por favor, digite uma senha!";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    obscureText: true,
                    controller: confirmePasswordController,
                    decoration:
                        const InputDecoration(labelText: "Confirme sua senha"),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Por favor, digite uma senha válida!";
                      } else if (value != passwordController.text) {
                        return "As senhas não coincidem!";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: ElevatedButton(
                      onPressed: cadastrarUser,
                      child: const Text("Cadastrar"),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
