import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocalizacaoPage extends StatefulWidget {
  const LocalizacaoPage({super.key});

  @override
  State<LocalizacaoPage> createState() => _LocalizacaoPageState();
}

class _LocalizacaoPageState extends State<LocalizacaoPage> {
  BitmapDescriptor markerLogo = BitmapDescriptor.defaultMarker;

  @override
  void initState() {
    super.initState();
    _iconeLogo();
    _permicao();
  }

  void _permicao() async {
    LocationPermission permission;
    bool ativado = await Geolocator.isLocationServiceEnabled();

    if (!ativado) {
      return Future.error('Habilite a localização do dispositivo');
    }
    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        return Future.error(
            'Permissão de localização negada. Autorize a localização.');
      }
    }
  }

//carrega o icone na localização do google
  void _iconeLogo() async {
    BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(devicePixelRatio: 2.5),
      "assets/logolaranja.png",
    ).then((icone) {
      setState(() {
        markerLogo = icone;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    LatLng posicaoInicial = const LatLng(-22.2208, -49.9486);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Localização'),
        centerTitle: true,
        backgroundColor: const Color(0xffF98E1E),
        elevation: 0,
      ),
      body: GoogleMap(
        myLocationButtonEnabled: true,
        myLocationEnabled: true,
        initialCameraPosition: CameraPosition(
          target: posicaoInicial,
          zoom: 16,
        ),
        markers: {
          Marker(
            markerId: const MarkerId('LojaIBurguer'),
            position: posicaoInicial,
            icon: markerLogo,
            onTap: () {
              showModalBottomSheet(
                context: context,
                builder: (context) {
                  return const SizedBox(
                    height: 100,
                    child: Text(
                      'Rua Paraiba, 125 - Centro',
                      style: TextStyle(fontSize: 18),
                    ),
                  );
                },
              );
            },
          ),
        },
      ),
    );
  }
}
