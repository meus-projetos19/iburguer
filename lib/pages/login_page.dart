import 'package:app_lanchonete/home.dart';
import 'package:app_lanchonete/pages/cadastrar_page.dart';
import 'package:flutter/material.dart';
import 'package:app_lanchonete/helpers/auth.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final _formkey = GlobalKey<FormState>();

  void loginUser() async {
    //teclado celular tirar o foco da tela do teclado
    FocusManager.instance.primaryFocus?.unfocus();
    if (_formkey.currentState!.validate()) {
      //debugPrint("Form Enviado!");

      bool isLogado =
          await Auth.login(emailController.text, passwordController.text);

      if (isLogado) {
        if (context.mounted) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const Home()),
          );
        }
      } else {
        if (context.mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Usuario ou senha Inválido'),
              backgroundColor: Colors.red,
            ),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Login"),
          backgroundColor: const Color(0xffF98E1E),
          elevation: 0,
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Form(
            key: _formkey,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/logolaranja.png',
                  ),
                  const SizedBox(height: 50),
                  TextFormField(
                    controller: emailController,
                    decoration: const InputDecoration(labelText: 'E-mail'),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Por favor, informe o e-mail!";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(labelText: 'Senha'),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Por favor, informe a senha!";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 20),
                  SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: ElevatedButton(
                      onPressed: loginUser,
                      child: const Text('Entrar'),
                    ),
                  ),
                  const SizedBox(height: 20),
                  const Text('Não possui uma conta?'),
                  // TextButton(
                  //   onPressed: () {},
                  //   child: const Text('Cadastre-se'),
                  // ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const CadastrarPage(),
                          ),
                        );
                      },
                      child: const Text(
                        'Cadastre-se',
                        style: TextStyle(color: Color(0xffF98E1E)),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
