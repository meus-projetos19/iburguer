import 'package:flutter/material.dart';
import 'package:app_lanchonete/models/produto.dart';

class ProdutoPage extends StatelessWidget {
  final Produto produto;
  const ProdutoPage({super.key, required this.produto});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/logotipo.png",
          height: 40,
        ),
        backgroundColor: const Color(0xffF98E1E),
        elevation: 0,
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        //style: const TextStyle(fontSize: 16),
        children: [
          Container(
            decoration: const BoxDecoration(
                // borderRadius: BorderRadius(20),
                ),
            child: AspectRatio(
              aspectRatio: 10 / 7,
              child: Image.network(
                produto.foto,
                fit: BoxFit.cover,
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            child: Text(
              produto.titulo,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            child: Text(
              produto.descricao,
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            child: Text(
              "R\$ ${produto.valor}",
              textAlign: TextAlign.left,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.red,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
