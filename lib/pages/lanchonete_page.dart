import 'package:flutter/material.dart';

class LanchonetePage extends StatelessWidget {
  const LanchonetePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/logotipo.png",
          height: 40,
        ),
        backgroundColor: const Color(0xffF98E1E),
        elevation: 0,
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AspectRatio(
            aspectRatio: 10 / 6,
            child: Image.asset(
              'assets/lanchonete1.jpg',
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          const Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
            child: Text(
                "Bem-vindo ao iBurguer, onde a experiência de comer hambúrgueres se torna uma verdadeira aventura culinária. Localizada em um espaço moderno e acolhedor, nossa lanchonete oferece uma atmosfera convidativa para os amantes da boa comida."),
          ),
          const Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
            child: Text(
                "Seja para um almoço rápido, um jantar casual ou apenas uma parada para matar a fome, o iBurguer é o destino certo. Satisfaça seus desejos por hambúrgueres incríveis e desfrute de uma experiência gastronômica única em nossa lanchonete. Venha nos visitar e descubra por que o iBurguer é o lugar onde sabor e qualidade se encontra."),
          ),
          const Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
            child: Text("Formas de Pagamento:"),
          )
        ],
      ),
    );
  }
}
