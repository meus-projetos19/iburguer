import 'dart:convert';

import 'package:app_lanchonete/helpers/api_url.dart';
import 'package:app_lanchonete/models/produto.dart';
import 'package:app_lanchonete/pages/produto_page.dart';
import 'package:flutter/material.dart';
import '../models/categoria.dart';
import '../widgets/box_icon_categoria.dart';
import 'package:http/http.dart' as http;
//comentário
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Future<List<Categoria>> _listaCategoria;
  late Future<List<Produto>> _listaProdutos;
  //  List produtos = [];

  @override
  void initState() {
    super.initState();

    _listaCategoria = carregarCategorias();
    _listaProdutos = carregarProdutos();
  }

  Future<void> filtrarProdutos(
      {int? categoriaId, String? termoPesquisa}) async {
    _listaProdutos = carregarProdutos();

    if (categoriaId != null) {
      setState(() {
        _listaProdutos = _listaProdutos.then((produtos) => produtos
            .where((produto) => produto.categoriaId == categoriaId)
            .toList());
      });
    }

//filtra do banco de dados
    if (termoPesquisa != null && termoPesquisa.isNotEmpty) {
      setState(() {
        _listaProdutos = _listaProdutos.then((produtos) => produtos
            .where((produto) =>
                produto.titulo
                    .toLowerCase()
                    .contains(termoPesquisa.toLowerCase()) ||
                produto.descricao
                    .toLowerCase()
                    .contains(termoPesquisa.toLowerCase()))
            .toList());
      });
    }
  }

  Future<List<Categoria>> carregarCategorias() async {
    final response = await http.get(Uri.parse("${ApiUrl.baseUrl}/categorias"));
    if (response.statusCode == 200) {
      List listaCategorias = json.decode(response.body);

      debugPrint(listaCategorias.toString());

      List<Categoria> categorias = listaCategorias
          .map((categoria) => Categoria.fromJson(categoria))
          .toList();
      return categorias;
    } else {
      throw Exception('Falha ao carregar as categorias');
    }
  }

  Future<List<Produto>> carregarProdutos() async {
    final response = await http.get(Uri.parse("${ApiUrl.baseUrl}/produtos"));
    if (response.statusCode == 200) {
      List listaProdutos = json.decode(response.body);

      List<Produto> produtos =
          listaProdutos.map((produto) => Produto.fromJson(produto)).toList();
      return produtos;
    } else {
      throw Exception('Falha ao carregar produtos');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: SingleChildScrollView(
        child: Column(
          children: [
            TextField(
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction
                  .search, //troca os icones do TECLADO do USUARIO
              decoration: InputDecoration(
                //helperText: "Texto de Ajuda", //coloca texto em baixo do campo / DICA
                //labelText: "Pesquisar", //coloca dica em CIMA e no MEIO
                hintText: "Quer comer o que?",
                suffixIcon: const Icon(Icons.search),
                //prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
              ),
              onChanged: (texto) {
                debugPrint(texto);
                //captura informação do usuario e armazena
              },
              onSubmitted: (texto) {
                debugPrint("Clicou em pesquisar");
                debugPrint(texto);
                filtrarProdutos(termoPesquisa: texto);
              },
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 80,
              child: FutureBuilder<List<Categoria>>(
                future: _listaCategoria,
                builder: (context, snapshotCategorias) {
                  if (snapshotCategorias.hasData) {
                    final categorias = snapshotCategorias.data;

                    return ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: categorias?.length,
                      itemBuilder: (context, index) {
                        final categoria = categorias![index];

                        return GestureDetector(
                          onTap: () {
                            filtrarProdutos(categoriaId: categoria.id);
                          },
                          child: BoxIconCategoria(
                            asset: categoria.icone,
                            color: Color(int.parse(categoria.cor)),
                            label: categoria.titulo,
                          ),
                        );
                      },
                    );
                  } else if (snapshotCategorias.hasError) {
                    debugPrint("$snapshotCategorias.error}");
                  }
                  return const Center(
                    child: CircularProgressIndicator.adaptive(),
                  );
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),

            //Função Future retorna produtos
            FutureBuilder<List<Produto>>(
              future: _listaProdutos,
              builder: (context, snapshotProdutos) {
                if (snapshotProdutos.hasData) {
                  final produtos = snapshotProdutos.data;

                  return GridView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: produtos?.length,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 10,
                            crossAxisSpacing: 10,
                            childAspectRatio: 10 / 15.5),
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        //faz a chamada de página
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ProdutoPage(
                                produto: produtos[index],
                              ),
                            ),
                          );
                        },
                        child: Card(
                          color: const Color(0xffffffff),
                          clipBehavior: Clip.hardEdge,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AspectRatio(
                                aspectRatio: 10 / 8.5,
                                child: Image.network(
                                  produtos![index].foto,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              const SizedBox(
                                height: 2,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    Text(
                                      produtos[index].titulo,
                                      style: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff343434),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      produtos[index].descricao,
                                      style: const TextStyle(
                                        fontSize: 12,
                                        color: Color(0xff343434),
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                    ),
                                    const SizedBox(
                                      height: 6,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "R\$ ${produtos[index].valor.toStringAsFixed(2)}",
                                          style: const TextStyle(
                                            color: Colors.red,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                          ),
                                        ),
                                        // SizedBox(
                                        //   width: 30,
                                        //   child: FloatingActionButton.small(
                                        //     onPressed: () {
                                        //       //gesturedetector
                                        //     },
                                        //     elevation: 0, //remove a sombra do botão
                                        //     backgroundColor: Colors.red,
                                        //     child: const Icon(Icons.add),
                                        //   ),
                                        // ),

                                        SizedBox(
                                          width: 30,
                                          child: ElevatedButton(
                                            onPressed: () {},
                                            style: ElevatedButton.styleFrom(
                                              padding: EdgeInsets.zero,
                                              shape: const CircleBorder(),
                                            ),
                                            child: const Icon(Icons.add),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                } else if (snapshotProdutos.hasError) {
                  debugPrint("$snapshotProdutos.error");
                }
                return const Center(
                  child: CircularProgressIndicator.adaptive(),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
