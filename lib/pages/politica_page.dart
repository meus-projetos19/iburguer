import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PoliticaPage extends StatefulWidget {
  const PoliticaPage({super.key});

  @override
  State<PoliticaPage> createState() => _PoliticaPageState();
}

class _PoliticaPageState extends State<PoliticaPage> {
  var controllerWeb = WebViewController()
    ..setJavaScriptMode(JavaScriptMode.unrestricted)
    ..loadRequest(
      Uri.parse("https://google.com"),
    );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Politica de Privacidade'),
      ),
      body: WebViewWidget(controller: controllerWeb),
    );
  }
}
