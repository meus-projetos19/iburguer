import 'package:app_lanchonete/home.dart';
import 'package:app_lanchonete/pages/login_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xffF98E1E)),
        scaffoldBackgroundColor: Colors.white,
      ),
      home: const Home(),
    ),
  );
}
