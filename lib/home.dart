import 'package:app_lanchonete/helpers/auth.dart';
import 'package:app_lanchonete/pages/cesta_page.dart';
import 'package:app_lanchonete/pages/favoritos_page.dart';
import 'package:app_lanchonete/pages/home_page.dart';
import 'package:app_lanchonete/pages/pedidos_page.dart';
import 'package:app_lanchonete/pages/conta_page.dart';
import 'package:app_lanchonete/widgets/menu_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _paginaIndex = 0;

  final List<Widget> paginas = [
    const HomePage(),
    const CestaPage(),
    const FavoritosPage(),
    const PedidosPage(),
    const ContaPage(),
  ];

//verifica se o usuario esta logado
  @override
  void initState() {
    super.initState();
    Auth.chegarLogin(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "assets/logotipo.png",
          height: 40,
        ),
        backgroundColor: const Color(0xffF98E1E),
        elevation: 0,
        centerTitle: true,
        actions: [
          // IconButton(onPressed: () {}, icon: const Icon(Icons.))
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: IconButton(
              onPressed: () async {
                var whatsappUrl = 'https://wa.me/5514988161422?text-mensagemdetexto';
                if (await launchUrl(Uri.parse(whatsappUrl))) {
                  throw 'Could not launch $whatsappUrl';
                }
              },
              icon: SvgPicture.asset(
                'assets/icon-whatsapp2.svg',
                height: 28,
                colorFilter:
                    const ColorFilter.mode(Colors.white, BlendMode.srcIn),
              ),
            ),
          )
        ],
      ),
      drawer: const MenuDrawer(), //menu separado em arquivo
      body: paginas[_paginaIndex],
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color(0xffF98E1E),
          selectedItemColor: const Color.fromARGB(255, 255, 255, 255),
          currentIndex: _paginaIndex,
          iconSize: 30,
          onTap: (index) {
            //debugPrint(index.toString());

            setState(() {
              _paginaIndex = index;
            });
          },
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.shopping_bag,
              ),
              label: "Cesta",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.favorite,
              ),
              label: "Favoritos",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.receipt,
              ),
              label: "Pedidos",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
              ),
              label: "User",
            ),
          ]),
    );
  }
}
