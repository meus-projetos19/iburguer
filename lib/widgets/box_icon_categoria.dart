import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BoxIconCategoria extends StatelessWidget {
  final String asset;
  final Color color;
  final String label;

  const BoxIconCategoria({
    super.key,
    required this.asset,
    required this.color,
    required this.label,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 10),
      height: 80,
      width: 80,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: const Color(0xFFF3F3F3),
          border: Border.all(color: const Color(0xffececec)),
          borderRadius: BorderRadius.circular(20)),
      child: Column(
        children: [
          SvgPicture.network(
            asset,
            colorFilter: ColorFilter.mode(color, BlendMode.srcIn),
            width: 45,
          ),
          Text(
            label,
            style: const TextStyle(
              fontSize: 10,
              fontWeight: FontWeight.bold,
              color: Color(0xff707070),
            ),
          )
        ],
      ),
    );
  }
}
