import 'package:app_lanchonete/pages/lanchonete_page.dart';
import 'package:app_lanchonete/pages/localizacao_page.dart';
import 'package:app_lanchonete/pages/politica_page.dart';
import 'package:flutter/material.dart';

import '../helpers/auth.dart';

class MenuDrawer extends StatelessWidget {
  const MenuDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.orange,
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: [
                SizedBox(
                  height: 150,
                  child: DrawerHeader(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: Divider.createBorderSide(context,
                            color: Colors.orange, width: 2.0),
                      ),
                    ),
                    //padding: const EdgeInsets.only(top: 8, bottom: 8),
                    padding: EdgeInsets.zero,
                    child: Image.asset(
                      'assets/logotipo.png',
                      height: 10,
                    ),
                  ),
                ),
                ListTile(
                  leading: const Icon(
                    Icons.fastfood,
                    color: Colors.white,
                  ),
                  title: const Text(
                    "Lanchonete",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const LanchonetePage(),
                      ),
                    );
                    //colocar uma tela de visualização da lanchonete...
                  },
                ),
                ListTile(
                  leading: const Icon(
                    Icons.location_on,
                    color: Colors.white,
                  ),
                  title: const Text(
                    "Localização",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const LocalizacaoPage(),
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: const Icon(
                    Icons.contacts_sharp,
                    color: Colors.white,
                  ),
                  title: const Text(
                    "Contato",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                  onTap: () {
                    //colocar uma tela de visualização da lanchonete...
                  },
                ),
                const Divider(),
                ListTile(
                  title: const Text(
                    "Politica de Privacidade",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const PoliticaPage(),
                      ),
                    );
                  },
                ),
                ListTile(
                  title: const Text(
                    "Sair",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                  onTap: () {
                    Auth.logout(context);
                  },
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              "Versão 1.0",
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
