import 'dart:convert';

import 'package:app_lanchonete/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../helpers/api_url.dart';

//funções estaticas de instancia
class Auth {
  static Future<bool> login(String email, String password) async {
    var response = await http.post(
      Uri.parse("${ApiUrl.baseUrl}/login"),
      body: {
        'email': email,
        'password': password,
      },
    );

    var usuario = json.decode(response.body);

    if (response.statusCode == 200) {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setString('email', usuario['email']);
      prefs.setString('nome', usuario['nome']);
      prefs.setString('token', usuario['token']);
      prefs.setString('imagem', usuario['imagem']);

      return true;
    }

    return false;
  }

  static Future<void> logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();

    if (context.mounted) {
      //destroi a tela anterior pra não ter que ficar voltando...
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => const LoginPage()),
      );
    }
  }

//token de login em cache
  static Future<void> chegarLogin(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? token = prefs.getString('token');

    if (token == null) {
      if (context.mounted) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const LoginPage()),
        );
      }
    }
  }
}
